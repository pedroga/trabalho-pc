#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <time.h>

typedef double **Matrix;

int N = 0, N0;
int pid, size;
MPI_Status st;
Matrix d;

void matrix_print(Matrix m);
Matrix matrix_create(int lines, int columns);
Matrix matrix_scan(char *ifname);

void floyd_warshall(void)
{
    int i, j, k;
    double *link = malloc(sizeof(double) * N0);
    Matrix d1 = matrix_create(N, N0), dk, da;

    dk = d;
    for (k = 0; k < N0; ++k) {
	if (pid == k % size) {
	    MPI_Bcast(dk[k/size], N0, MPI_DOUBLE, pid, MPI_COMM_WORLD);
	    for (i = 0; i < N0; ++i)
		link[i] = dk[k/size][i];
	} else
	    MPI_Bcast(link, N0, MPI_DOUBLE, k % size, MPI_COMM_WORLD);


	da = dk;
	dk = (dk == d ? d1 : d);
	for (i = 0; i < N; ++i)
	    for (j = 0; j < N0; ++j)
		dk[i][j] = fmin(da[i][j], da[i][k] + link[j]);
    }
    d = dk;
}

int main(int argc, char **argv)
{
    char *ifname;
    Matrix d0;
    int i, j;

    if (argc < 2) {
	printf("Faltando argumento obrigatorio: nome do arquivo");
	exit(1);
    }
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (!pid) {
	ifname = argv[1];
	d0 = matrix_scan(ifname);
    }
    MPI_Bcast(&N0, 1, MPI_INT, 0, MPI_COMM_WORLD);
    for (i = 0; i < N0; ++i)
	if (i % size == pid) ++N;
    if (!pid) {
	for (i = 0; i < N0; ++i) {
	    if (i % size)
		MPI_Send(d0[i], N0, MPI_DOUBLE, i % size, 0, MPI_COMM_WORLD);
	}
	d = matrix_create(N, N0);
	for (i = 0; i < N; ++i)
	    for (j = 0; j < N0; ++j)
		d[i][j] = d0[i * size][j];
    } else {
	d = matrix_create(N, N0);
	for (i = 0; i < N; ++i)
	    MPI_Recv(d[i], N0, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &st);
    }
    clock_t begin;
    if (!pid) {
    	begin = clock();
    }
    floyd_warshall();
    if (!pid) {
        clock_t end = clock();
        double time_spent = (double)(end - begin);
        printf("%.lf", time_spent);
    }

    if (!pid) {
	for (i = 0; i < N0; ++i)
	    if (i % size)
		MPI_Recv(d0[i], N0, MPI_DOUBLE, i % size, 1, MPI_COMM_WORLD, &st);
	for (i = 0; i < N; ++i)
	    for (j = 0; j < N0; ++j)
		d0[i * size][j] = d[i][j];
    } else {
	for (i = 0; i < N; ++i)
	    MPI_Send(d[i], N0, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    }
    MPI_Finalize();
}


void matrix_print(Matrix m)
{
    int i, j;

    for (i = 0; i < N0; ++i) {
	for (j = 0; j < N0; ++j)
	    printf("[%.2lf]", m[i][j]);
	puts("");
    }
    puts("");
}

Matrix matrix_create(int lines, int columns)
{
    Matrix m = malloc(lines * sizeof(double *));
    int i, j;
    
    for (i = 0; i < lines; ++i)
	m[i] = malloc(columns * sizeof(double));
    for (i = 0; i < lines; ++i)
	for (j = 0; j < lines; ++j)
	    m[i][j] = INFINITY;
    return m;
}

Matrix matrix_scan(char *ifname)
{
    FILE *fp;
    int vertices = 0, i;
    char line[255];
    Matrix m;

    if (!(fp = fopen(ifname, "r"))) {
	printf("Nao foi possivel abrir o arquivo\n");
	exit(1);
    }
    fgets(line, 255, fp);
    while (fgets(line, 255, fp) && line[0] != '#')
	++vertices;
    N0 = vertices;
    m = matrix_create(vertices, vertices);
    while (fgets(line, 255, fp)) {
    	int i, j;
    	double w;
	
    	sscanf(line, "%d %d %lf", &i, &j, &w);
    	m[i - 1][j - 1] = w;
    }
    for (i = 0; i < N0; ++i)
	m[i][i] = 0;
    return m;
}
