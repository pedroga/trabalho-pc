#!/bin/bash

PROG=$1
SIZE=500

while [[ "$SIZE" -le "5000" ]]; do
    SUM=0
    for j in $(seq 1 13); do
        ID=$(( 14 - j ))
        if [ "$j" -eq "1" ]; then
            MIN=$(./$PROG grafo_$SIZE.g $THREADS)
            MIN=$(( MIN + 0 ))
            MAX=$(( MIN + 0 ))
        else
            TIMES[ID]=$(./$PROG grafo_$SIZE.g $THREADS $i $j)
            TIMES[ID]=$(( ${TIMES[$ID]} + 0 ))
            if [ "${TIMES[$ID]}" -le "$MIN" ]; then
                MIN=$ID
            fi
            if [ "${TIMES[$ID]}" -ge "$MAX" ]; then
                MAX=$ID
            fi
        fi
    done

    for j in $(seq 1 12); do
        if [ "${TIMES[$j]}" -ne "$MIN" ]; then
            if [ "${TIMES[$j]}" -ne "$MAX" ]; then
                SUM=$(( SUM + ${TIMES[$j]} + 0 ))
            fi
        fi
    done
    
    DEC=$(bc -l <<< "$SUM / 100000000000")
    SEQ=$DEC
    DEC=$(bc -l <<< "$DEC * 10")
    echo -e "\n\n\t\t\x1B[33mSequential $SIZE:\t\t\t\x1B[31mTime: $DEC\n"
    echo -e "\x1B[0m\n"
    printf "%s\t%s\n" "$SIZE" "$DEC" >> results/$PROG.dat
    SIZE=$(( SIZE + 500))
done