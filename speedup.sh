#!/bin/bash

PROG=$1
SIZE=500
THREADS=16
INDEX=1
SEQ=(.0008841589 .0070465145 .0242404594 .057058501 .1111776703 .1890654116 .2985656132 .443779849 .6795504339 .9149291944)
END=$2

while [[ "$THREADS" -le "$END" ]]; do
    INDEX=0
    while [[ "$SIZE" -le "5000" ]]; do
        SUM=0
        for j in $(seq 1 13); do
            ID=$(( 14 - j ))
            if [ "$j" -eq "1" ]; then
		    MIN=$(./run $THREADS grafo_$SIZE.g $i $j)
                MIN=$(( MIN + 0 ))
                MAX=$(( MIN + 0 ))
            else
                TIMES[ID]=$(./run $THREADS grafo_$SIZE.g $i $j)
                TIMES[ID]=$(( ${TIMES[$ID]} + 0 ))
                if [ "${TIMES[$ID]}" -le "$MIN" ]; then
                    MIN=$ID
                fi
                if [ "${TIMES[$ID]}" -ge "$MAX" ]; then
                    MAX=$ID
                fi
            fi
        done

        for j in $(seq 1 12); do
            if [ "${TIMES[$j]}" -ne "$MIN" ]; then
                if [ "${TIMES[$j]}" -ne "$MAX" ]; then
                    SUM=$(( SUM + ${TIMES[$j]} + 0 ))
                fi
            fi
        done 
        TSEQ=${SEQ[$INDEX]}
        DEC=$(bc -l <<< "$SUM / 100000000000")
        DEC=$(bc -l <<< "$DEC * 10")
        SPD=$(bc -l <<< "($TSEQ / $DEC) * $THREADS")
        echo -e "\t\x1B[33mPthread:\t\t\t\x1B[31mTime: $(bc -l <<< "$DEC / $THREADS")\t\x1B[36mSpeedup: $SPD"
        printf "%s\t%s\t%s\t%s\t\n" "$SIZE" "$THREADS" "$DEC" "$SPD" >> results/$PROG.dat
        SIZE=$(( $SIZE + 500 ))
        INDEX=$(( INDEX + 1 ))
    done
    THREADS=$(( THREADS * 2 ))
done
