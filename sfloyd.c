#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int N;

typedef double **Matrix;

#define TEST 0

void matrix_print(Matrix m)
{
    int i, j;

    for (i = 0; i < N; ++i)
    {
        printf("|");
        for (j = 0; j < N; ++j)
            printf("[%.2lf]", m[i][j]);
        printf("|\n");
    }
    printf("\n");
}

Matrix matrix_create(int lines)
{
    Matrix m = malloc(lines * sizeof(double **));
    int i, j;

    for (i = 0; i < lines; ++i)
        m[i] = malloc(lines * sizeof(double *));
    for (i = 0; i < lines; ++i)
        for (j = 0; j < lines; ++j)
            m[i][j] = INFINITY;
    return m;
}

Matrix matrix_dup(Matrix m)
{
    Matrix new = malloc(N * sizeof(double **));
    ;
    int i, j;

    for (i = 0; i < N; ++i)
    {
        new[i] = malloc(N * sizeof(double *));
        for (j = 0; j < N; ++j)
            new[i][j] = m[i][j];
    }
    return new;
}

Matrix matrix_scan(char *ifname)
{
    FILE *fp;
    int vertices = 0, i;
    char line[255];
    Matrix m;

    if (!(fp = fopen(ifname, "r")))
    {
        printf("Nao foi possivel abrir o arquivo\n");
        exit(1);
    }
    fgets(line, 255, fp);
    while (fgets(line, 255, fp) && line[0] != '#')
        ++vertices;
    N = vertices;
    m = matrix_create(vertices);
    while (fgets(line, 255, fp))
    {
        int i, j;
        double w;

        sscanf(line, "%d %d %lf", &i, &j, &w);
        m[i - 1][j - 1] = w;
    }
    for (i = 0; i < N; ++i)
        m[i][i] = 0;
    return m;
}

Matrix floyd_warshall(Matrix w)
{
    Matrix d0 = matrix_dup(w);
    Matrix d1 = matrix_create(N);
    Matrix dk = d0, d;

    int i, j, k;
    for (i = 0; i < N; ++i)

        for (k = 0; k < N; ++k)
        {
            d = dk;
            dk = (dk == d0 ? d1 : d0);
            for (i = 0; i < N; ++i)
                for (j = 0; j < N; ++j)
                    dk[i][j] = fmin(d[i][j], d[i][k] + d[k][j]);
        }
    return dk;
}

int main(int argc, char **argv)
{
    char *ifname;

    if (argc < 2)
    {
        printf("Modo de uso: %s <nome do arquivo de entrada>\n",
               argv[0]);
        exit(1);
    }
    ifname = argv[1];
#if TEST
    matrix_print(floyd_warshall(matrix_scan(ifname)));
#else
    clock_t begin = clock();
    free(floyd_warshall(matrix_scan(ifname)));
    clock_t end = clock();
    double time_spent = (double)(end - begin);
    printf("%.lf", time_spent);
#endif
}
